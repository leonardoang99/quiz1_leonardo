﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_Leonardo.Entities
{
    public class TasDbContext:DbContext
    {
        public TasDbContext(DbContextOptions<TasDbContext> options) : base(options)
        {

        }
        public DbSet<Tas> Tass { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tas>().ToTable("tass");

        }
    }
}
