﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Quiz1_Leonardo.Entities
{
    public class Tas
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TasId { get; set; }
        [Required]
        public string TasName { get; set; }
        [Required]
        public int JumlahTas { get; set; }
        
    }
}
