﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_Leonardo.Entities
{
    public class TasDbContextFactory :  IDesignTimeDbContextFactory<TasDbContext>
    {
        public TasDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TasDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new TasDbContext(builder.Options);
        }
    }
}
