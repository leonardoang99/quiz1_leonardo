﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Quiz1_Leonardo.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tass",
                columns: table => new
                {
                    TasId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TasName = table.Column<string>(nullable: false),
                    JumlahTas = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tass", x => x.TasId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tass");
        }
    }
}
