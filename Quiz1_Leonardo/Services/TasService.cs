﻿using Microsoft.EntityFrameworkCore;
using Quiz1_Leonardo.Entities;
using Quiz1_Leonardo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Leonardo.Services
{
    public class TasService
    {
        /// <summary>
        /// panggil entities kedalam service
        /// </summary>
        private readonly TasDbContext _db;

        public TasService(TasDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// buat insertData
        /// </summary>
        /// <param name="tasModel"></param>
        /// <returns></returns>
        public async Task<bool> InsertTas(TasModel tasModel)
        {
            this._db.Add(new Tas
            {
                TasName = tasModel.TasName,
                JumlahTas = tasModel.JumlahTas
            });

            await this._db.SaveChangesAsync();
            return true;
        }
        /// <summary>
        /// buat ambil semua data tas
        /// </summary>
        /// <returns></returns>
        public async Task<List<TasModel>> GetAllTassAsync()
        {
            var employees = await this._db
                .Tass
                .Select(Q => new TasModel
                {
                    TasId = Q.TasId,
                    TasName = Q.TasName,
                    JumlahTas = Q.JumlahTas
                })
                .AsNoTracking()
                .ToListAsync();

            return employees;
        }
        /// <summary>
        /// buat ambil id tas yang mau di update
        /// </summary>
        /// <param name="tasId"></param>
        /// <returns></returns>
        public async Task<TasModel> GetSpesificTasAsync(int tasId)
        {
            var employee = await this._db
                .Tass
                .Where(Q => Q.TasId == tasId)
                .Select(Q => new TasModel
                {
                    TasId = Q.TasId,
                    TasName = Q.TasName,
                    JumlahTas = Q.JumlahTas
                })
                .FirstOrDefaultAsync();

            return employee;
        }
        /// <summary>
        /// update data tas setelah disubmit
        /// </summary>
        /// <param name="tas"></param>
        /// <returns></returns>
        public async Task<bool> UpdateTasAsync(TasModel tas)
        {
            var TasModel = await this._db
                .Tass
                .Where(Q => Q.TasId == tas.TasId)
                .FirstOrDefaultAsync();

            if (TasModel == null)
            {
                return false;
            }

            TasModel.TasName= tas.TasName;
            TasModel.JumlahTas = tas.JumlahTas;

            await this._db.SaveChangesAsync();

            return true;
        }
        /// <summary>
        /// delete data tas
        /// </summary>
        /// <param name="tasId"></param>
        /// <returns></returns>

        public async Task<bool> DeleteTasAsync(int tasId)
        {
            var tasModel = await this._db.Tass.Where(Q => Q.TasId == tasId).FirstOrDefaultAsync();

            if (tasModel == null)
            {
                return false;
            }

            this._db.Remove(tasModel);
            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// buat pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        public async Task<List<TasModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {

            var query = this._db
                .Tass
                .AsQueryable();

            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.TasName.StartsWith(filterByName));
            }

            var tass = await query
                .Select(Q => new TasModel
                {
                   TasId = Q.TasId,
                   TasName = Q.TasName,
                   JumlahTas = Q.JumlahTas
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return tass;
        }
        /// <summary>
        /// buat ambil total data 
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalTas = this._db
                .Tass
                .Count();

            return totalTas;
        }

    }
}
