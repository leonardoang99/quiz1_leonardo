﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quiz1_Leonardo.Models;
using Quiz1_Leonardo.Services;

namespace Quiz1_Leonardo.API
{
    [Route("api/v1/tas")]
    [ApiController]
    public class TasApiController : ControllerBase
    {
        private readonly TasService _serviceMan;

        public TasApiController(TasService tasService)
        {
            this._serviceMan = tasService;
        }

        [HttpGet("all-tas", Name = "allTas")]
        public async Task<IActionResult> GetAllTasAsync()
        {
            var tass = await _serviceMan.GetAllTassAsync();
            return Ok(tass);
        }

        [HttpPost("insert", Name = "insertTas")]
        public async Task<ActionResult<ResponseModel>> InsertNewTasAsync([FromBody]TasModel tas)
        {
            var isSuccess = await _serviceMan.InsertTas(tas);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data Tas."
            });
        }

        [HttpPut("update", Name = "updateTas")]
        public async Task<ActionResult<ResponseModel>> UpdateTasAsync([FromBody] TasModel modelTas)
        {
            var sukses = await _serviceMan.UpdateTasAsync(modelTas);

            if (sukses == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Tas Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {modelTas.TasName}"
            });
        }

        [HttpDelete("delete", Name = "deleteTas")]
        public async Task<ActionResult<ResponseModel>> DeleteTasAsync([FromBody] TasModel tasModel)
        {
            var _sukses = await _serviceMan.DeleteTasAsync(tasModel.TasId);

            if (_sukses == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Tas not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete Tas {tasModel.TasName}"
            });
        }

        [HttpDelete("delete-2/{TasId}", Name = "deleteTas2")] 
        public async Task<ActionResult<ResponseModel>> DeleteTas2Async(int tasId)
        {
            var isSuccess = await _serviceMan.DeleteTasAsync(tasId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID tas is not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete Tas"
            });
        }
        [HttpGet("specific-Tas", Name = "getSpesificTas")]
        public async Task<ActionResult<TasModel>> GetSpecificTasAsync(string tasId)
        {
            if (string.IsNullOrEmpty(tasId) == true)
            {
                return BadRequest(null);
            }

            var id = Int32.Parse(tasId);
            var tas = await _serviceMan.GetSpesificTasAsync(id);

            if (tas == null)
            {
                return BadRequest(null);
            }
            return Ok(tas);
        }

        [HttpGet("filter-dataTas")]
        public async Task<ActionResult<List<TasModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var dataTas = await _serviceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(dataTas);
        }

        [HttpGet("total-dataTas")]
        public ActionResult<int> GetTotalData()
        {
            var totalDataTas = _serviceMan.GetTotalData();

            return Ok(totalDataTas);
        }
    }
}