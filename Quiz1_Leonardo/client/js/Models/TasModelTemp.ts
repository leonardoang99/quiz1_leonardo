﻿export interface TasModelTemp {
    tasId?: number;
    tasName?: string;
    jumlahTas?: number;
}
export interface ResponseModelTemp {
    responseMessage: string;
}