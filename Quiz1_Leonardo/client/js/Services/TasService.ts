﻿import Axios from 'axios';
import { TasModelTemp, ResponseModelTemp } from '../Models/TasModelTemp';


//axios ambil dri API 

export class TasService {
    
    async getAllTas(): Promise<TasModelTemp[]> {
        const response = await (await Axios.get<TasModelTemp[]>('/api/v1/tas/all-tas')).data;
        if (response === null) {
            return [];
        }
        return response;

    }

    async insertTas(data: TasModelTemp): Promise<string> {
        const response = await Axios.post<ResponseModelTemp>('/api/v1/tas/insert', data);
        if (response.status === 200) {
            return response.data.responseMessage
        }
        return response.data.responseMessage;
    }

    async deleteTas(data: number): Promise<string> {
        const response = await (await (Axios.delete<ResponseModelTemp>('/api/v1/tas/delete-2/' + data))).data;
        if (response === null) {
            return '';
        }

        return response.responseMessage;
    }

    async updateTas(data: TasModelTemp): Promise<string> {
        const response = await Axios.put<ResponseModelTemp>('/api/v1/tas/update', data);
        if (response.status === 200) {
            return response.data.responseMessage
        }
        return response.data.responseMessage;
    }

    async getFilterData(data1: number, data2: number, data3: string): Promise<TasModelTemp[]> {
        const response = await (await Axios.get<TasModelTemp[]>('/api/v1/tas/filter-datatas?pageIndex' + data1 + '&itemPerPage=' + data2 + '&filterByName=' + data3)).data;

        if (response === null) {
            return [];
        }
        return response;
    }


}


export const TasSingleton = new TasService();