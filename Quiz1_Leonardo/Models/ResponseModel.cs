﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Leonardo.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// model buat responmessage
        /// </summary>
        public string ResponseMessage { set; get; }
        /// <summary>
        /// model buat status
        /// </summary>
        public string Status { set; get; }
    }

}
