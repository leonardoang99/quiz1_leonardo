﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Leonardo.Models
{
    public class TasModel
    {
        /// <summary>
        /// model buat tasId
        /// </summary>
        public int TasId { set; get; }
        /// <summary>
        /// model buat tasName
        /// </summary>
        [Required]
        [StringLength(7, MinimumLength = 3)]
        [Display(Name = "Nama Tas")]
        public string TasName { set; get; }
        /// <summary>
        /// model buat Jumlah Tas
        /// </summary>
        [Required]
        [Display(Name = "Jumlah Tas")]
        public int JumlahTas { set; get; }
    }
}
